package com.tsystems.javaschool.tasks.zones;

public class Edge {
    int first;
    int second;

    public Edge(int first, int second) {
        this.first = first;
        this.second = second;
    }

    public int getFirst() {
        return first;
    }

    public int getSecond() {
        return second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        return (first == edge.first && second == edge.second)
                || (first == edge.second && second == edge.first);
    }
}
