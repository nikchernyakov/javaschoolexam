package com.tsystems.javaschool.tasks.zones;

import java.util.*;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        // Create edges set of zone state
        Set<Edge> edges = new HashSet<>();
        zoneState.forEach(zone ->
                zone.getNeighbours().forEach(neighbour -> {
                    Edge edge = new Edge(zone.getId(), neighbour);
                    if(!edges.contains(edge)){
                        edges.add(edge);
                    }
                }));

        // Create array that shows is requested zones available
        Map<Integer, Boolean> availableRequestedZones = new HashMap<>();
        requestedZoneIds.forEach(zoneId -> availableRequestedZones.put(zoneId, false));

        // Check all edges
        edges.forEach(edge -> {
            if(requestedZoneIds.contains(edge.getFirst()) && requestedZoneIds.contains(edge.getSecond())){
                availableRequestedZones.replace(edge.getFirst(), false, true);
                availableRequestedZones.replace(edge.getSecond(), false, true);
            }
        });

        return !availableRequestedZones.values().contains(false);
    }
}
