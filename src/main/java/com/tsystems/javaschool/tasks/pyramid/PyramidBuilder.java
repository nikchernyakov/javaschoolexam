package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // Find pyramid's rows count
        /*
           Calculate by formula of arithmetic progression's sum
           S = (2 * a1 + d * (n - 1)) * n / 2
           where S - size of input list, a1 - elements count in the first pyramid row ie a1=1,
           d - difference between 2 rows of pyramid - d=1, n - rows count (what need to find)
        */
        double rowsCountDouble = (Math.sqrt(8 * inputNumbers.size() + 1) - 1) / 2; // Double for checking fractional part
        int rowsCount = (int) rowsCountDouble;

        // Check that rows count is integer and not null number and list of numbers doesn't contains a null
        if (rowsCount == 0 || rowsCount != rowsCountDouble || inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        // Set column count
        // Every new row has 2 new elements (d=1) and there is 1 element in the first row
        int columnCount = 1 + 2 * (rowsCount - 1);

        // Create 2d array
        int[][] pyramid = new int[rowsCount][columnCount];

        // Sort number's list
        inputNumbers.sort(Comparator.comparing(Integer::intValue));

        // Fill 2d array
        int columnDiv2 = columnCount / 2;
        Iterator<Integer> iterator = inputNumbers.iterator();
        for(int rowIndex = 0; rowIndex < rowsCount; rowIndex++){
            for(int columnIndex = columnDiv2 - rowIndex;
                columnIndex < columnCount - columnDiv2 + rowIndex; columnIndex += 2){
                pyramid[rowIndex][columnIndex] = iterator.next();
            }
        }

        return pyramid;
    }
}
